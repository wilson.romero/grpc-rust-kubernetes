# Microservices example

This repo is a hello world of kubernetes + docker + rust + grpc + linkerd + nginx ingress

## How to run

**Prerequisites**

- Install [minikube](https://minikube.sigs.k8s.io/docs/start/)
- Install kubectl
- Install docker
- Install [linkerd](https://linkerd.io/2.10/getting-started/)
- Enable ingress: ```minikube addons enable ingress```

**Hot to run**

- ```minikube start```
- ```./scripts/1-build-docker```
- ```./scripts/2-create-tls```
- ```./scripts/3-run-kubernetes```

**How to run dashboards**

- ```minikube dashboard```
- ```linkerd viz dashboard```

**How to do load tests**

- Install [ghz](https://ghz.sh/)
- ```./scripts/5-load-tests```

**How to stop everything**
- ```./scripts/4-stop-kubernetes```
- ```minikube stop```


